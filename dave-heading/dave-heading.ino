
// ------------------- Dave heading app - poczta@ekolives.com ---------------------

#include <Arduino.h>
#include <TM1637Display.h>



int Altcounter = 0;
int Headcounter = 0;
int HeadButtoncounter = 0;
int AltButtoncounter = 0;
int Altlicznik;
int Headlicznik;



int AltState;
int HeadState;
int AltLastState;
int HeadLastState;
int AltStateButton;
int HeadStateButton;




// Module connection pins (Digital Pins) pokretła 
#define AltoutputA 9
#define AltoutputB 8
#define AltButton 13
#define HeadingoutputA 7
#define HeadingoutputB 6
#define HeadButton 12


// Module connection pins (Digital Pins) wyswietlacze 
#define AltCLK 4
#define AltDIO 5

#define HeadingCLK 10
#define HeadingDIO 11

TM1637Display display1(AltCLK, AltDIO);// define dispaly 1 object
TM1637Display display2(HeadingCLK, HeadingDIO);// define dispaly 1 object

void setup()
{
  Serial.begin (9600);

  pinMode (AltoutputA, INPUT);
  pinMode (AltoutputB, INPUT);
  AltLastState = digitalRead(AltoutputA);

  pinMode (HeadingoutputA, INPUT);
  pinMode (HeadingoutputB, INPUT);
  HeadLastState = digitalRead(HeadingoutputA);

  pinMode (AltButton, INPUT);
  AltStateButton = digitalRead(AltButton);

  pinMode (HeadButton, INPUT);
  HeadStateButton = digitalRead(HeadButton);



}

void loop() {
  HeadingEncoder();
  AltitudeEncoder();

  HeadingButton();
  AltitudeButton();
}


//--------------

// delay(200); \
//    Serial.begin(115200); \
//    FlightSim.update(); \
//    Serial.printf(F("%10lu: Start " __FILE__ ", compiled on " __DATE__ " at " __TIME__ "\n"), millis()); \
// ma być nazwa z dataref i wartosc --- chyba  -- Serial.write(nav1.asBytes[i]);

//-------------------    

void AltitudeEncoder() {

  AltState = digitalRead(AltoutputA); // Reads the "current" state of the outputA

  // If the previous and the current state of the outputA are different, that means a Pulse has occured
  if (AltState != AltLastState and AltState == 0 ) {
    // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
    if (digitalRead(AltoutputB) != AltState and AltState == 0) {


      if (AltButtoncounter == 1) {
        Altcounter ++;
      } else {
        for (Altlicznik = 1; Altlicznik < 11; ++Altlicznik) Altcounter++;
      }

      if (Altcounter >= 4500) {
        Altcounter = 000;
      }
    } else {
      if (AltButtoncounter == 1) {
        Altcounter --;
      } else {
        for (Altlicznik = 1; Altlicznik < 11; ++Altlicznik) Altcounter--;
      }

      if (Altcounter < 0) {
        Altcounter = 4500;
      }
    }
    Serial.println("sim/cockpit/autopilot/altitude,"FLOAT", "Altcounter);


    //Serial.print("Altitude Position: ");
    //Serial.println(Altcounter);

    display1.setBrightness(3);
    display1.showNumberDec(Altcounter, true);
  }
  AltLastState = AltState; // Updates the previous state of the outputA with the current state
}

void HeadingEncoder() {

  HeadState = digitalRead(HeadingoutputA); // Reads the "current" state of the outputA

  // If the previous and the current state of the outputA are different, that means a Pulse has occured
  if (HeadState != HeadLastState and HeadState == 0 ) {
    // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
    if (digitalRead(HeadingoutputB) != HeadState and HeadState == 0) {

      if (HeadButtoncounter == 1) {
        Headcounter ++;
      } else {
        for (Headlicznik = 1; Headlicznik < 11; ++Headlicznik) Headcounter++;
      }
      if (Headcounter >= 360) {
        Headcounter = 000;
      }
    } else {

      if (HeadButtoncounter == 1) {
        Headcounter --;
      } else {
        for (Headlicznik = 1; Headlicznik < 11; ++Headlicznik) Headcounter--;
      }
      if (Headcounter < 0) {
        Headcounter = 359;
      }
    }
    Serial.print("Heading Position: ");
    Serial.println(Headcounter);

    display2.setBrightness(3);
    display2.showNumberDec(Headcounter, true);
  }
  HeadLastState = HeadState; // Updates the previous state of the outputA with the current state

}

void HeadingButton() {

  HeadStateButton = digitalRead(HeadButton);
  if (HeadStateButton == 0) {
    HeadButtoncounter ++;
    if (HeadButtoncounter == 2) {
      HeadButtoncounter = 0;
    }
    Serial.print("HeadStateButton selected=");
    Serial.println(HeadButtoncounter);
    delay (300);
  }
}

void AltitudeButton() {
  AltStateButton = digitalRead(AltButton);
  if (AltStateButton == 0) {
    AltButtoncounter ++;
    if (AltButtoncounter == 2) {
      AltButtoncounter = 0;
    }
    Serial.print("AltStateButton selected=");
    Serial.println(AltButtoncounter);
    delay (300);
  }
}
